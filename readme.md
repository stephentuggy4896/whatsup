#WhatsUp

##Last Update
May 1, 2014

##Summary
WhatsUp is used to generate a wiki document listing all the online/offline devices received from a Pelco system manager.

##Building
```sh
git clone https://blackey02@bitbucket.org/blackey02/whatsup.git
git clone https://blackey02@bitbucket.org/blackey02/intops.git
```
* WhatsUp requires the DeviceRegister library from IntOps/PelcoApi/DeviceRegister
* Open WhatsUp.sln and add the DeviceRegister projct to the solution

##Configuration
* In the App.config file (or WhatsUp.exe.config) you have the following settings
```sh
<setting name="SMUsername" serializeAs="String">
    <value>admin</value>
</setting>
<setting name="SMPassword" serializeAs="String">
    <value>admin</value>
</setting>
<setting name="SMPort" serializeAs="String">
    <value>60001</value>
</setting>
<setting name="SMIP" serializeAs="String">
    <value>192.168.5.10</value>
</setting>
<setting name="WikiOutro" serializeAs="String">
    <value>Wiki Footer</value>
</setting>
<setting name="WikiIntro" serializeAs="String">
    <value>Wiki Header</value>
</setting>
<setting name="Devices" serializeAs="Xml">
    <value>
        <ArrayOfString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <string>Alarm</string>
            <string>AlarmArray-disabled</string>
            <string>Basic-disabled</string>
            <string>Camera</string>
            ...
        </ArrayOfString>
    </value>
</setting>
```
* The '-disabled' removes this device from being searched

##Screens
![](http://i.imgur.com/OO54UVj.png)
![](http://i.imgur.com/qWvQRAO.png)
![](http://i.imgur.com/GmYNfJi.png)
![](http://i.imgur.com/tBWF3PW.png)