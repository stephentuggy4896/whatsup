﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace WhatsUp
{
    class Wiki
    {
        private Dictionary<string, List<DeviceRegister.DeviceInfo>> _devices;

        public Wiki()
        {
            _devices = new Dictionary<string, List<DeviceRegister.DeviceInfo>>();
        }

        private string DeviceListHeader
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append("|| {color:#222222}Device Name{color} || {color:#222222}Model{color} || ");
                sb.Append("{color:#222222}Private IP{color} || {color:#222222}Login{color} || ");
                sb.Append("{color:#222222}Current &nbsp;Status{color} || {color:#222222}Extra{color} ||");
                sb.Append("{color:#222222}Thumbnail{color} ||");
                return sb.ToString();
            }
        }

        public bool AddDevice(DeviceRegister.DeviceInfo deviceInfo)
        { 
            if(!deviceInfo.Attributes.ContainsKey("SYS_UpnpDeviceType"))
                return false;

            string type = deviceInfo.Attributes["SYS_UpnpDeviceType"];
            if (!_devices.ContainsKey(type))
                _devices.Add(type, new List<DeviceRegister.DeviceInfo>());
            _devices[type].Add(deviceInfo);
            return true;
        }

        public bool CreateWikiPage()
        {
            var sb = new StringBuilder();
            sb.AppendLine("{excerpt}");
            sb.AppendLine(Properties.Settings.Default.WikiIntro);

            foreach (string type in _devices.Keys)
            {
                //_devices[type].Sort();  // TODO: Sort by what?
                
                string friendlyType = type;
                var matches = Regex.Matches(friendlyType, @"(?<=:)[\w-]+");
                if (matches != null && matches.Count >= 3)
                    friendlyType = matches[2].Value;

                string typeTemplate = "h5. {color:#222222}[0]{color}";
                typeTemplate = typeTemplate.Replace("{", "{{");
                typeTemplate = typeTemplate.Replace("}", "}}");
                typeTemplate = typeTemplate.Replace("[", "{");
                typeTemplate = typeTemplate.Replace("]", "}");
                sb.AppendLine(string.Format(typeTemplate, friendlyType));
                sb.AppendLine(DeviceListHeader);

                foreach (DeviceRegister.DeviceInfo info in _devices[type])
                {
                    string defaultValue = "Unknown";
                    string username = "admin";
                    string password = "admin";
                    string friendlyName, model, ip, status, extra = string.Empty, thumbnail = string.Empty;
                    friendlyName = model = ip = status = defaultValue;

                    if (info.Attributes.ContainsKey("friendlyName"))
                        friendlyName = info.Attributes["friendlyName"];
                    else if (info.Attributes.ContainsKey("SYS_UpnpFriendlyName"))
                        friendlyName = info.Attributes["SYS_UpnpFriendlyName"];

                    if (info.Attributes.ContainsKey("SYS_UpnpModelNumber"))
                        model = info.Attributes["SYS_UpnpModelNumber"];
                    else if (info.Attributes.ContainsKey("SYS_UpnpModelName"))
                        model = info.Attributes["SYS_UpnpModelName"];

                    if (info.Attributes.ContainsKey("SYS_UpnpDevDescUrl"))
                    {
                        var match = Regex.Match(info.Attributes["SYS_UpnpDevDescUrl"], @"(?<=\/\/)[\d.]+");
                        if (match.Success)
                            ip = match.Value;
                    }

                    status = Enum.GetName(typeof(DeviceRegister.UpnpState), info.State);

                    if (info.Attributes.ContainsKey("CameraType"))
                    {
                        int cameraType = int.Parse(info.Attributes["CameraType"]);
                        switch (cameraType)
                        {
                            case 0: extra = "Type: Fixed"; break;
                            case 1: extra = "Type: PanTilt"; break;
                            case 2: extra = "Type: Dome"; break;
                            default: extra = string.Format("Type: {0} ({1})", defaultValue, cameraType); break;
                        }
                    }

                    if (info.Attributes.ContainsKey("CameraNumber"))
                    {
                        string channelNumber = string.Format("Number: {0}", info.Attributes["CameraNumber"]);
                        extra = string.IsNullOrEmpty(extra) ? channelNumber : (extra + @"\\" + channelNumber);
                    }

                    bool isLocalIp = ip.Contains("192.168");

                    if (!isLocalIp && info.State == DeviceRegister.UpnpState.Online && (type == "urn:schemas-pelco-com:device:Camera:1" /* || type == "urn:schemas-pelco-com:device:Encoder:1" */ ))
                        thumbnail = string.Format("!http://{0}/jpeg?id=2|border=1,width=200!", ip);

                    // Do Last
                    if (!isLocalIp)
                        ip = string.Format("[{0}|http://{0}]", ip);

                    string deviceTemplate = @"| {color:#222222}[0]{color} | {color:#222222}[1]{color} | {color:#222222}[2]{color} | {color:#222222}Username: [3]{color}\\  {color:#222222}Password: [4]{color} | {color:#222222}[5]{color} | {color:#222222}[6]{color} | {color:#222222}[7]{color} |";
                    deviceTemplate = deviceTemplate.Replace("{", "{{");
                    deviceTemplate = deviceTemplate.Replace("}", "}}");
                    deviceTemplate = deviceTemplate.Replace("[", "{");
                    deviceTemplate = deviceTemplate.Replace("]", "}");
                    sb.AppendLine(string.Format(deviceTemplate, friendlyName, model, ip, username, password, status, extra, thumbnail));
                }
            }

            sb.AppendLine(Properties.Settings.Default.WikiOutro);
            sb.AppendLine("{excerpt}");
            WriteToDesktop(sb.ToString());
            return true;
        }

        private bool WriteToDesktop(string data)
        {
            string filename = "whatsup";
            string filenameExt = ".txt";
            string filePath = string.Empty;
            bool exists;
            int counter = 0;
            do
            {
                string desktopDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                filePath = counter == 0 ? string.Format("{0}{1}", filename, filenameExt) : string.Format("{0}{1}{2}", filename, counter, filenameExt);
                filePath = Path.Combine(desktopDir, filePath);
                exists = File.Exists(filePath);
                counter++;
            } while (exists);
            File.WriteAllText(filePath, data);
            return true;
        }
    }
}
