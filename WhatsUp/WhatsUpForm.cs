﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WhatsUp
{
    public partial class WhatsUpForm : Form
    {
        private Core _core;

        public WhatsUpForm()
        {
            InitializeComponent();
            _core = new Core();
        }

        private void WhatsUpForm_Load(object sender, EventArgs e)
        {
            _core.CreateWikiPage();
            this.Close();
        }
    }
}
