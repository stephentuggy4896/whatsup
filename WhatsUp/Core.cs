﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhatsUp
{
    class Core
    {
        private DeviceRegister.SmCredentials _cred;
        private Dictionary<string, DeviceRegister.DeviceInfo> _devices;
        private Wiki _wiki;

        public Core()
        {
            _cred = new DeviceRegister.SmCredentials()
            {
                Ip = Properties.Settings.Default.SMIP,
                Password = Properties.Settings.Default.SMPassword,
                Username = Properties.Settings.Default.SMUsername,
                Port = Properties.Settings.Default.SMPort
            };
            _devices = new Dictionary<string, DeviceRegister.DeviceInfo>();
            _wiki = new Wiki();
        }

        public bool CreateWikiPage()
        {
            bool success = false;
            if (GetDevices())
            {
                foreach (DeviceRegister.DeviceInfo info in _devices.Values)
                    _wiki.AddDevice(info);
                success = _wiki.CreateWikiPage();
            }
            return success;
        }

        private bool GetDevices()
        {
            bool success = false;
            var register = new DeviceRegister.Register(_cred);
            if (register.Open())
            {
                _devices = register.GetDevices(ImportTypes());
                register.Close();
                success = true;
            }
            return success;
        }

        private DeviceRegister.DeviceType[] ImportTypes()
        {
            var deviceTypes = new List<DeviceRegister.DeviceType>();
            foreach (string type in Properties.Settings.Default.Devices)
            { 
                switch(type.ToLower())
                {
                    case "alarm": deviceTypes.Add(DeviceRegister.DeviceType.Alarm); break;
                    case "alarmarray": deviceTypes.Add(DeviceRegister.DeviceType.AlarmArray); break;
                    case "basic": deviceTypes.Add(DeviceRegister.DeviceType.Basic); break;
                    case "camera": deviceTypes.Add(DeviceRegister.DeviceType.Camera); break;
                    case "decoder": deviceTypes.Add(DeviceRegister.DeviceType.Decoder); break;
                    case "encoder": deviceTypes.Add(DeviceRegister.DeviceType.Encoder); break;
                    case "eventarbiter": deviceTypes.Add(DeviceRegister.DeviceType.EventArbiter); break;
                    case "gateway": deviceTypes.Add(DeviceRegister.DeviceType.Gateway); break;
                    case "mappingui": deviceTypes.Add(DeviceRegister.DeviceType.MappingUI); break;
                    case "mdd": deviceTypes.Add(DeviceRegister.DeviceType.MDD); break;
                    case "monitor": deviceTypes.Add(DeviceRegister.DeviceType.Monitor); break;
                    case "mddmonitor": deviceTypes.Add(DeviceRegister.DeviceType.MDDMonitor); break;
                    case "nsd": deviceTypes.Add(DeviceRegister.DeviceType.NSD); break;
                    case "networkvideorecorder": deviceTypes.Add(DeviceRegister.DeviceType.NetworkVideoRecorder); break;
                    case "nsm": deviceTypes.Add(DeviceRegister.DeviceType.NSM); break;
                    case "pelco": deviceTypes.Add(DeviceRegister.DeviceType.Pelco); break;
                    case "relay": deviceTypes.Add(DeviceRegister.DeviceType.Relay); break;
                    case "relayarray": deviceTypes.Add(DeviceRegister.DeviceType.RelayArray); break;
                    case "remoteui": deviceTypes.Add(DeviceRegister.DeviceType.RemoteUI); break;
                    case "storageexpansionbox": deviceTypes.Add(DeviceRegister.DeviceType.StorageExpansionBox); break;
                    case "systemmanagerlocatordevice": deviceTypes.Add(DeviceRegister.DeviceType.SystemManagerLocat0rDevice); break;
                    case "scriptmanager": deviceTypes.Add(DeviceRegister.DeviceType.ScriptManager); break;
                    case "systemmanagerdevice": deviceTypes.Add(DeviceRegister.DeviceType.SystemManagerDevice); break;
                    case "systemlogdevice": deviceTypes.Add(DeviceRegister.DeviceType.SystemLogDevice); break;
                    case "transcoder": deviceTypes.Add(DeviceRegister.DeviceType.Transcoder); break;
                    case "universaldeviceinput": deviceTypes.Add(DeviceRegister.DeviceType.UniversalDeviceInput); break;
                    case "virtualalarmarray": deviceTypes.Add(DeviceRegister.DeviceType.VirtualAlarmArray); break;
                    case "vcd": deviceTypes.Add(DeviceRegister.DeviceType.VCD); break;
                    default: break;
                }
            }
            return deviceTypes.ToArray();
        }
    }
}
